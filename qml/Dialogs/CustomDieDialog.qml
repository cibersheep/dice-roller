import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.Popups 1.3

import "../Components"

Dialog {
    id: customDicePopup

    property alias name: nameField.text
    property var values: []
    onValuesChanged: {
        valuesField.text = values.join(', ');
    }

    signal saved(string name, var values)

    function colorIsInvalid(colorToCheck) {
        var colorList = ["aliceblue","antiquewhite","aqua","aquamarine","azure","beige","bisque","black","blanchedalmond","blue","blueviolet","brown","burlywood","cadetblue","chartreuse","chocolate","coral","cornflowerblue","cornsilk","crimson","cyan","darkblue","darkcyan","darkgoldenrod","darkgray","darkgreen","darkgrey","darkkhaki","darkmagenta","darkolivegreen","darkorange","darkorchid","darkred","darksalmon","darkseagreen","darkslateblue","darkslategray","darkslategrey","darkturquoise","darkviolet","deeppink","deepskyblue","dimgray","dimgrey","dodgerblue","firebrick","floralwhite","forestgreen","fuchsia","gainsboro","ghostwhite","gold","goldenrod","gray","grey","green","greenyellow","honeydew","hotpink","indianred","indigo","ivory","khaki","lavender","lavenderblush","lawngreen","lemonchiffon","lightblue","lightcoral","lightcyan","lightgoldenrodyellow","lightgray","lightgreen","lightgrey","lightpink","lightsalmon","lightseagreen","lightskyblue","lightslategray","lightslategrey","lightsteelblue","lightyellow","lime","limegreen","linen","magenta","maroon","mediumaquamarine","mediumblue","mediumorchid","mediumpurple","mediumseagreen","mediumslateblue","mediumspringgreen","mediumturquoise","mediumvioletred","midnightblue","mintcream","mistyrose","moccasin","navajowhite","navy","oldlace","olive","olivedrab","orange","orangered","orchid","palegoldenrod","palegreen","paleturquoise","palevioletred","papayawhip","peachpuff","peru","pink","plum","powderblue","purple","red","rosybrown","royalblue","saddlebrown","salmon","sandybrown","seagreen","seashell","sienna","silver","skyblue","slateblue","slategray","slategrey","snow","springgreen","steelblue","tan","teal","thistle","tomato","turquoise","violet","wheat","white","whitesmoke","yellow","yellowgreen"];

        var chkVal = colorToCheck.toLowerCase()
        chkVal = chkVal.replace(" ","")
        var test = colorList.indexOf(chkVal);
        return (test >= 0) ? false : true;
    }

    function fixUnicode(faceValue) {
        var valuePos = faceValue.toLowerCase().indexOf("u+");
        var unicodeValue = parseInt(faceValue.substr(valuePos+2), 16)
        var unicodeString = String.fromCharCode(unicodeValue);

        if(faceValue.indexOf("]")>0) {
            var valuePos = faceValue.indexOf("]")
            return faceValue.substr(0,valuePos + 1) + unicodeString;
        } else {
            return unicodeString;
        }
    }

    function save() {
        var v = valuesField.text.split(',');
        var v2 = [];
        var value = v[0].trim();
        if(value.indexOf("]")>0) {
            var valuePos = value.indexOf("]")
            var mylabelColor = value.substr(1,valuePos-1)
            if(colorIsInvalid(mylabelColor)) {
                PopupUtils.open(badColorDialogComponent, root);
            }
        }

        for (var i = 0; i < v.length; i++) {
            value = v[i].trim();
            if(value.toLowerCase().indexOf("u+")> -1) {
                value = fixUnicode(value);
             }

            if (value) {
                v2.push(value);
            }
        }

        if (v2.length > 0 && nameField.text.length > 0) {
            saved(nameField.text, v2);
            PopupUtils.close(customDicePopup);
        }
    }

    Label {
        text: i18n.tr('Create a custom die')
        horizontalAlignment: Label.AlignHCenter
    }

    Label {
        text: i18n.tr('Name')
        horizontalAlignment: Label.AlignHCenter
        textSize: Label.Small
    }

    TextField {
        id: nameField
    }

    Label {
        text: i18n.tr('Enter side values separated by commas. You can enter emojis directly from the keyboard or use unicode for extra symbols and foreign characters,\ne.g. U+278A, U+278B, U+278C ,U+278D\n\nDots are special and produce a standard die. Precede first value with an SVG color name if required,\ne.g. [coral].,.,.,.,.,.)');

        horizontalAlignment: Label.AlignHCenter
        textSize: Label.Small
        wrapMode: Text.WordWrap
    }

    TextField {
        id: valuesField
        inputMethodHints: Qt.ImhNoPredictiveText

        onAccepted: save()
    }

    Button {
        text: i18n.tr('Save')
        color: UbuntuColors.orange

        onClicked: save()
    }

    Button {
        text: i18n.tr('Cancel')

        onClicked: PopupUtils.close(customDicePopup)
    }
}